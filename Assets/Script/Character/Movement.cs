﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    public float Speed = 10f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    protected abstract void Move();
}

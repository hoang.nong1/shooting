﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kill : MonoBehaviour
{
    // Start is called before the first frame update
    public float TimeToDie;


    private void OnEnable()
    {
        StartCoroutine("Destruction");
    }

    IEnumerator Destruction()
    {
        yield return new WaitForSeconds(TimeToDie);
        Destroy(gameObject);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Character : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    protected string CharName;
    protected double Power = 1;
    protected int Health = 2;
    protected int Level { get ; set ;}
    protected GameObject destructionVFX;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void SetPower()
    {
        if(String.Equals(CharName, "Player"))
        {
            Power *= Level;
        }
        else
        {
            Power += Level;
        }
    }

    public void GetHealth()
    {
        
    }

    protected void SetHealth()
    {
        if(String.Equals(CharName, "Player"))
        {
            Health += Level;
        }
        else
        {
            Health = Health - 1 + Level;
        }
    }

    protected void DestroyChar()
    {
        Instantiate(destructionVFX, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    public bool CheckTypeCharacter(string Type)
    {
        return CharName.Equals(Type);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class CharController : MonoBehaviour
{
    // Start is called before the first frame update
    public Character Character { get ; set; }
    public Movement Movement { get ; set; }
    public Shooting Shooting { get ; set; }

    void Awake()
    {
        Character = GetComponent<Character>();
        Movement = GetComponent<Movement>();
        Shooting = GetComponent<Shooting>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

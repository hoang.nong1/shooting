﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Start is called before the first frame update
    public static GameManager instance;

    public MapController MapController { get ; set; }
    public Player Player { get; set; }
    public MenuController MenuController { get ; set ; }
    public RoundController RoundController { get ; set ; }
    public MainCamera MainCamera { get ; set ; }
    public static Camera camera;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        MapController = GetComponent<MapController>();
        Player = GetComponent<Player>();
        MenuController = GetComponent<MenuController>();
        RoundController = GetComponent<RoundController>();
        MainCamera = GetComponent<MainCamera>();
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

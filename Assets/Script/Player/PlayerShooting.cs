﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Bullet;
    ParticleSystem Gun;
    private float nextFire = 0;
    private float fireRate = 10f;

    GameObject clone;
    void Awake()
    {
        Gun = gameObject.GetComponentInChildren<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextFire)
        {
            Shooting();                                                     
            nextFire = Time.time + 10 / fireRate;
        }
        if(Time.time > 20)
        {
            Destroy(clone);
        }
    }

    private void Shooting()
    {
        CreateBullet(Bullet, Gun.transform.position, Vector3.zero);
    }

    private void CreateBullet(GameObject bullet, Vector3 pos, Vector3 rot)
    {
        clone = Instantiate(bullet, pos, Quaternion.Euler(rot));
    }

}

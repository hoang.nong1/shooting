﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : Movement
{
    // Start is called before the first frame update

    public Camera mainCamera;
    bool controlIsActive = true; 

    private void Awake()
    {

    }
    void Start()
    {
        
    }

    // Update is called once per frame
    protected override void Move()
    {
           if (controlIsActive)
        {
#if UNITY_STANDALONE || UNITY_EDITOR    //if the current platform is not mobile, setting mouse handling 

            if (Input.GetMouseButton(0)) //if mouse button was pressed       
            {
                Vector3 mouse = mainCamera.ScreenToWorldPoint(Input.mousePosition); //calculating mouse position in the worldspace
                mouse.z = transform.position.z;
                transform.position = Vector3.MoveTowards(transform.position, mouse, Speed * Time.deltaTime);
            }
#endif

#if UNITY_IOS || UNITY_ANDROID //if current platform is mobile, 

            if (Input.touchCount == 1) // if there is a touch
            {
                Touch touch = Input.touches[0];
                Vector3 touchPosition = mainCamera.ScreenToWorldPoint(touch.position);  //calculating touch position in the world space
                touchPosition.z = transform.position.z;
                transform.position = Vector3.MoveTowards(transform.position, touchPosition, Speed * Time.deltaTime);
            }
#endif
        }
    }

}


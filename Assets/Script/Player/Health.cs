﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject[] HealthObject;
    private Image[] HealthImages;
    private int NumOfHealth = 3;
    private Player Player;

    void Awake()
    {
        HealthObject = GetComponentsInChildren<GameObject>();
        for(int i = 0; i < HealthObject.Length; i++)
        {
            HealthImages[i] = HealthObject[i].GetComponent<Image>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HealthUpdate()
    {
        
    }

}

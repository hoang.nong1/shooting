﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    // Start is called before the first frame update
    public Player()
    {
        CharName = "Player";
        Level = 1;
        SetPower();
        SetHealth();
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetDamage()
    {
        Health -= 1;
    }

}

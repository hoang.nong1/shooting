﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    // Start is called before the first frame update
    void Start()
    {
        CharName = "Enemy";
        Level = 1;
        SetPower();
        SetHealth();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
